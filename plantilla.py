#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):
    productions = []
    for i in range(min_trees,max_trees + 1):
        opcion = (i, compute_trees(i))
        productions.append(opcion)
    productions = []

    return productions

def read_arguments():
   if len(sys.argv) == 6:
       try:
            base_trees = int(sys.argv[1])
            fruit_per_tree = int(sys.argv[2])
            reduction = int(sys.argv[3])
            min = int(sys.argv[4])
            max = int(sys.argv[5])
            return base_trees, fruit_per_tree, reduction, min, max
       except:
           print("All arguments must be integers")
   else:
       print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
#sys.argv = ["40", "5", "4", "4", "10"]
#read_arguments()

def main():

    ...
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    ...
    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
