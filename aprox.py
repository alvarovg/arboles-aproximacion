#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):
    productions = []
    for i in range(min_trees, max_trees + 1):
        opcion = (i, compute_trees(i))
        productions.append(opcion)

    return productions

def read_arguments():
   if len(sys.argv) == 6:
       try:
            base_trees = int(sys.argv[1])
            fruit_per_tree = int(sys.argv[2])
            reduction = int(sys.argv[3])
            min = int(sys.argv[4])
            max = int(sys.argv[5])
            return base_trees, fruit_per_tree, reduction, min, max
       except:
           print("All arguments must be integers")
           raise SystemExit
   else:
       print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
       raise SystemExit

def main():
    global base_trees
    global reduction
    global fruit_per_tree

    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)

    best_production = 0
    for i in range(0, len(productions)):
        if productions[i][1] > best_production:
            best_production = productions[i][1]
            tupla = i

    best_trees = productions[tupla][0]

    for t in productions:
        print(*t)
    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()